#!/usr/bin/env python3
from pymongo import MongoClient
import sys
import os
from dotenv import load_dotenv

try:
    f = open(".env")
    load_dotenv(dotenv_path=".env")
except IOError:
    load_dotenv(dotenv_path=".env.example")

# Define IP Address vars and dash
try:
    ipAddr = sys.argv[1]
except:
    print("IP Address is required")
    exit(1)

dash = "=" *75

# Define auth and URI for mongo connection
authUser = os.getenv('MONGO_AUTH_USER')
authPassword = os.getenv('MONGO_AUTH_PASSWORD')
dbAddress = ipAddr

uri = "mongodb://" + authUser + ":" + authPassword +"@"+ dbAddress + ":27017/?authSource=admin"

# Find Users
dbConn = MongoClient(uri)
dbName = dbConn["admin"]
colls = dbName["system.users"]
result = colls.find({},{"user": 1,"db": 1,"roles": 1})

# Find Hostname
hostnameDBName = dbConn["local"]
hostnameColls = hostnameDBName["startup_log"]
hostDB = hostnameColls.find({},{"hostname": 1}).limit(1)

# Print Hostname
try:
    for hostDB in hostDB:
        print("Hostname: ")
        print(hostDB['hostname'])
except:
    print ("Cannot Connect to Mongo, Wrong Auth?")

# Print Users
print(dash)
print("username".ljust(25)+"database".ljust(30)+"roles".rjust(20))
print(dash)

try:
    for result in result:
        print(result['user'].ljust(25)+ result['db'].ljust(30) + result['roles'][0]['role'].rjust(20))
except:
    print("Cannot Connect to Mongo, Wrong Auth?")