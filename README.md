# Mongo Userlist

### Purpose
This snippet is intended to help you list all database users within your MongoDB servers

### Requirements
- `python 3`
- `pymongo`
- `python-dotenv`

### How to Use
- create .env file 
- run with `python3 mongo_userlist.py {MONGO_IP_ADDR}`

### Example
```python3 mongo_userlist.py 127.0.0.1```

Result
```
Hostname:
localhost
===========================================================================
username                 database                                     roles
===========================================================================
admin                    admin                                     __system
development              omegalul                                 readWrite
```